﻿using Newtonsoft.Json;
using Server.JsonSupport;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using NLog;

namespace Server
{
    public class ServerSettings
    {
        public string Ipv4 { get; set; }
        public int portNum { get; set; }
        [JsonIgnore]
        public IPEndPoint tcpEndPoint { get; set; }

        public ServerSettings()
        {

            Regex reg = new Regex("((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
            var addresses = Dns.GetHostEntry(Dns.GetHostName()).AddressList
                                                               .Select(el => el.ToString() + "|");
            var matches = reg.Matches(String.Concat(addresses));
            if (matches.Count > 1)
                throw new Exception("There are more 1 match to Ipv4");
            else
                Ipv4 = matches[0].ToString();
            portNum = 7000;
            tcpEndPoint = new IPEndPoint(IPAddress.Parse(Ipv4), portNum);
        }

        public ServerSettings(string ipv4, int portNum)
        {
            Ipv4 = ipv4;
            this.portNum = portNum;
            tcpEndPoint = new IPEndPoint(IPAddress.Parse(Ipv4), portNum);
        }

        public ServerSettings(string ipv4, int portNum, IPEndPoint tcpEndPoint)
        {
            Ipv4 = ipv4;
            this.portNum = portNum;
            this.tcpEndPoint = tcpEndPoint;
        }
        public static ServerSettings GetSettings()
        {
            string content = null;
            string path = Environment.CurrentDirectory.Replace(Path.Combine("bin", "Debug"), "Config");
            if (!File.Exists(Path.Combine(path, "ServerSettings.json")))
            {
                LogManager.GetCurrentClassLogger().Debug("Попытка создать новый файл ServerSettings.json");
                ServerSettings setts = new ServerSettings();
                content = JsonConvert.SerializeObject(setts, JsonSettings.GetJsonSettings());
                File.WriteAllText(Path.Combine(path, "ServerSettings.json"), content, Encoding.UTF8);
                return setts;
            }
            else
            {
                content = File.ReadAllText(Path.Combine(path, "ServerSettings.json"), Encoding.UTF8);
                if (!String.IsNullOrEmpty(content))
                {
                    LogManager.GetCurrentClassLogger().Debug("Считывание сеттингов из файла");
                    return JsonConvert.DeserializeObject<ServerSettings>(content, JsonSettings.GetJsonSettings());
                }
                else
                {
                    LogManager.GetCurrentClassLogger().Debug("Запись сеттингов в пустой файл");
                    ServerSettings setts = new ServerSettings();
                    content = JsonConvert.SerializeObject(setts, JsonSettings.GetJsonSettings());
                    File.WriteAllText(Path.Combine(path, "ServerSettings.json"), content, Encoding.UTF8);
                    return setts;
                }

            }
        }
    }
}
