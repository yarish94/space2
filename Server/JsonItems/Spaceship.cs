﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.JsonItems
{
    class Spaceship
    {
        [JsonProperty("X")]
        public int X { get; set; }
        [JsonProperty("Y")]
        public int Y { get; set; }
        [JsonProperty("IsAlive")]
        public bool IsAlive { get; set; }
        [JsonProperty("Image")]
        public int Image { get; set; }
        [JsonProperty("Id")]
        public int Id { get; set; }

        public static int[] size = new int[2] { 50, 50 };
        public int[] Size
        {
            get
            {
                return size;
            }
        }
        public int dS
        {
            get
            {
                return 25;
            }
        }
    }
}
