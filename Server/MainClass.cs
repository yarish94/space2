﻿using Newtonsoft.Json;
using Server.JsonSupport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Server
{
    internal static class MainClass
    {
        private static Socket tcpSocket = null;
        private static ServerSettings settings = ServerSettings.GetSettings();
        internal static Dictionary<API, byte[]> data = new Dictionary<API, byte[]>();
        internal static ManualResetEvent connectDone = new ManualResetEvent(false);


        static void Main(string[] args)
        {
            LogManager.GetCurrentClassLogger().Debug($"Старт работы сервера, версия: {Assembly.GetExecutingAssembly().GetName().Version.ToString()}");
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            TryBind(settings.tcpEndPoint);
            LogManager.GetCurrentClassLogger().Debug("Старт прослушивания");
            tcpSocket.Listen(5);

            while (true)
            {
                var listener = tcpSocket.Accept();
                API client = new API(ref listener);
                LogManager.GetCurrentClassLogger().Debug($"Соединение с {listener.RemoteEndPoint}");

            }
        }
        private static string GetIp(Socket socket)
        {
            string ipPattern = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}";
            return Regex.Match(socket.RemoteEndPoint.ToString(), ipPattern).ToString();
        }
        private static void TryBind(IPEndPoint iPEndPoint)
        {
            try
            {
                LogManager.GetCurrentClassLogger().Debug($"Попытка соединения с конечной точкой: {iPEndPoint}");
                tcpSocket.Bind(iPEndPoint);
            }
            catch (SocketException ex)
            {
                LogManager.GetCurrentClassLogger().Warn($"Не удалось подключиться к конечной точке: {iPEndPoint}");
                LogManager.GetCurrentClassLogger().Debug("Удаление файла сеттингов и создание нового");
                var path = Path.Combine(Environment.CurrentDirectory.Replace(Path.Combine("bin", "Debug"), "Config"), "ServerSettings.json");
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                settings = new ServerSettings();
                tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                LogManager.GetCurrentClassLogger().Debug($"Попытка повторного соединения с конечной точкой: {settings.tcpEndPoint}");
                try
                {
                    tcpSocket.Bind(settings.tcpEndPoint);
                }
                catch(Exception exFatal)
                {
                    LogManager.GetCurrentClassLogger().Fatal(exFatal.ToString());
                }
            }
            LogManager.GetCurrentClassLogger().Debug($"Успешное соединение c конечной точкой: {settings.tcpEndPoint}");
        }
    }
}
