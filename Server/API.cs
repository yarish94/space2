﻿using Newtonsoft.Json;
using NLog;
using Server.JsonItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class API
    {
        internal int Id { get; set; }
        internal Socket socket { get; }
        internal byte[] Buffer { get; set; }

        public API(ref Socket socket)
        {
            this.socket = socket;
            Buffer = new byte[256];
            Thread thread = new Thread(Initialization);
            thread.Start();
        }
        private void Initialization()
        {
            byte[] buffer = new byte[256];
            LogManager.GetCurrentClassLogger().Debug($"Инициализация клиента {socket.RemoteEndPoint}");
            socket.Receive(buffer);
            var temp = Encoding.UTF8.GetString(buffer);
            this.Id = Int32.Parse(Encoding.UTF8.GetString(buffer));
            var APIbyId = MainClass.data.Keys.Where(el => el.Id == Id).FirstOrDefault();
            buffer = new byte[256];
            if (APIbyId != null)
            {
                //Reconnection case below
                buffer = MainClass.data[APIbyId];
                MainClass.data.Remove(APIbyId);
            }
            MainClass.data.Add(this, buffer);
            SendCurrentData();
            ClientWork();
        }
        private void SendCurrentData()
        {
            //не работает нихуя
            foreach (var el in MainClass.data.Keys)
            {
                if (el != this)
                {
                    socket.Send(MainClass.data[el]);
                }

            }
        }
        private void ClientWork()
        {
            int bufferSize = 0;
            try
            {
                //int i = 0;
                while (true)
                {
                    do
                    {
                        bufferSize = socket.Receive(Buffer);

                    } while (socket.Available > 0);
                    if (Buffer != null)
                    {
                        SendToAllTheClients(Buffer);
                    }
                    bufferSize = 0;
                    Buffer = new Byte[256];
                    //i++;
                    //if (i == 10) throw new SocketException();
                }
            }
            catch (SocketException ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Разорвано соединение с {socket.RemoteEndPoint}");
                socket.Shutdown(SocketShutdown.Both);
                socket.Dispose();
            }
        }

        public void SendToAllTheClients(byte[] buffer)
        {
            foreach (var el in MainClass.data.Keys.ToArray())
            {
                if (el != this)
                {
                    el.socket.Send(buffer);
                    LogManager.GetCurrentClassLogger().Trace($"Передача сообщения от {el.socket.LocalEndPoint}");
                }
                else
                {
                    MainClass.data[el] = Buffer;
                }
            }
        }

    }
}
