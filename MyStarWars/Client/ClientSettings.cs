﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using Server.JsonSupport;

namespace MyStarWars.Client
{
    class ClientSettings
    {

        public string Ipv4 { get; set; }
        public int portNum { get; set; }
        [JsonIgnore]
        public IPEndPoint tcpEndPoint { get; set; }

        public ClientSettings(string ipv4, int portNum)
        {
            Ipv4 = ipv4;
            this.portNum = portNum;
            tcpEndPoint = new IPEndPoint(IPAddress.Parse(Ipv4), portNum);
        }
        public ClientSettings(string ipv4, int portNum, IPEndPoint tcpEndPoint)
        {
            Ipv4 = ipv4;
            this.portNum = portNum;
            this.tcpEndPoint = tcpEndPoint;
        }
        public ClientSettings()
        {
        }
        public static ClientSettings GetSettings()
        {
            string content = "";
            string path = Environment.CurrentDirectory.Replace(Path.Combine("bin", "Debug"), "Config");
            if (!File.Exists(Path.Combine(path, "ClientSettings.json")))
            {
                File.WriteAllText(Path.Combine(path, "ClientSettings.json"), content);
                throw new Exception("You must fill the gaps correctly in the file: ClientSettings.json");
            }
            else
            {
                content = File.ReadAllText(Path.Combine(path, "ClientSettings.json"), Encoding.UTF8);
                if (!String.IsNullOrEmpty(content))
                {
                    var temp = JsonConvert.DeserializeObject<ClientSettings>(content, JsonSettings.GetJsonSettings());
                    temp.tcpEndPoint = new IPEndPoint(IPAddress.Parse(temp.Ipv4), temp.portNum);
                    return temp;
                }
                else
                {
                    File.WriteAllText(Path.Combine(path, "ClientSettings.json"), content);
                    throw new Exception("You must fill the gaps correctly in the file: ClientSettings.json");
                }
            }
        }
    }
}
