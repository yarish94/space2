﻿using MyStarWars.Client;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStarWars
{
    class MyClient
    {
        static Socket tcpSocket;
        private static ClientSettings settings = ClientSettings.GetSettings();
        internal event EventHandler<string> Update;
        internal event EventHandler ReconnectStarted;
        internal event EventHandler ReconnectFinished;
        internal event EventHandler InitDone;
        internal static ManualResetEvent connectDone = new ManualResetEvent(false);
        internal bool isReconnecting = false;
        private Thread thread;
        private Task recieveTask;
        private int id;


        public MyClient(int id)
        {
            ReconnectStarted += MyClient_ReconnectStarted;
            ReconnectFinished += MyClient_ReconnectFinished;
            try
            {
                this.id = id;
                Connect();
                connectDone.Reset();
                recieveTask = new Task(() => Recieve());
                recieveTask.Start();
                thread = new Thread(new ParameterizedThreadStart(SendId));
                thread.Start(true);
                connectDone.Set();
            }
            catch (SocketException)
            {
                throw new Exception("Check ClientSetting");
            }
        }

        private void MyClient_ReconnectFinished(object sender, EventArgs e)
        {
            isReconnecting = false;
            recieveTask = new Task(() => Recieve());
            recieveTask.Start();
        }

        private void MyClient_ReconnectStarted(object sender, EventArgs e)
        {
        }

        private void SendId(Object isInit)
        {
            if (isInit is bool)
            {
                var parameter = (bool)isInit;
                connectDone.WaitOne();
                tcpSocket.Send(Encoding.UTF8.GetBytes(id.ToString()));
                if (parameter)
                    InitDone?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }

        }
        private async void StartWorking()
        {
            recieveTask = new Task(() => Recieve());
        }
        private void Recieve()
        {
            try
            {
                while (true)
                {
                    byte[] buffer = new byte[256];
                    int size = 0;
                    StringBuilder data = new StringBuilder();
                    do
                    {
                        size = tcpSocket.Receive(buffer);
                        data.Append(Encoding.UTF8.GetString(buffer, 0, size));

                    } while (tcpSocket.Available > 0);
                    if (Update != null && !String.IsNullOrEmpty(data.ToString()))
                        Update(this, data.ToString());
                }
            }
            catch (SocketException)
            {
                SocketExceptionHandler();
            }
        }
        public void SendData(string data)
        {
            if (!String.IsNullOrEmpty(data))
            {
                try
                {
                    tcpSocket.Send(Encoding.UTF8.GetBytes(data));
                }
                catch (SocketException)
                {
                    SocketExceptionHandler();
                }
                catch (ObjectDisposedException) { }
            }
            else
            {
                throw new Exception("Попытка отправки пустого сообщения на сервер!");
            }
        }
        private static void Connect()
        {
            connectDone.Reset();
            if(tcpSocket != null)
            {
                tcpSocket.Dispose();
            }
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            tcpSocket.BeginConnect(settings.tcpEndPoint, new AsyncCallback(ConnectCallBack), tcpSocket);
            connectDone.WaitOne();
        }
        private static void ConnectCallBack(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndConnect(ar);
            }
            catch (SocketException)
            {
                //хуячь логгер на клиенте!
            }
            catch (ObjectDisposedException) { }
            finally
            {
                connectDone.Set();
            }
        }
        private void SocketExceptionHandler()
        {
            if (isReconnecting) return;
            isReconnecting = true;
            DialogResult result = DialogResult.Yes;
            if (!tcpSocket.Connected)
            {
                result = MessageService.YesNoDialog("Connection lost", "Do you want to try reconnecting?");
            }
            switch (result)
            {
                case DialogResult.No:
                    Application.Exit();
                    break;
                case DialogResult.Yes:
                    connectDone.Reset();
                    ReconnectStarted?.Invoke(this, EventArgs.Empty);
                    do
                    {
                        Connect();
                    } while (!tcpSocket.Connected);
                    SendId(false);
                    ReconnectFinished?.Invoke(this, EventArgs.Empty);
                    break;
                default:
                    throw new Exception("Unknown error occurs. If you are here, you must RnD this problem.");
            }

        }
    }
}
