﻿using System.Windows.Forms;

namespace MyStarWars
{
    interface IMessageService
    {
        void ShowMessage(string message);
        void ShowError(string message);
    }

    internal static class MessageService
    {
        internal static void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        internal static void ShowMessage(string message)
        {
            MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        internal static void ShowPauseDialog()
        {
            MessageBox.Show("Please, wait for reconnecting", "Pause", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        internal static DialogResult ShowConnectionError(string message)
        {
            return MessageBox.Show(message, "ConnectionError", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
        }
        internal static DialogResult YesNoDialog (string title, string message)
        {
            return MessageBox.Show(message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

    }
}
