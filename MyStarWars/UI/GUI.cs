﻿using MyStarWars.Items;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static MyStarWars.UI.WaitingForm;

namespace MyStarWars.UI
{
    public interface IGameFieldForm
    {
        IItem[] Ships { get; set; }
    }


    public partial class GUI : Form
    {

        public event EventHandler<string> SendData;
        private bool waitUntilKeyUp = false;
        internal static Panel gameField;
        Stopwatch T = new Stopwatch();
        internal static ManualResetEvent waiter = new ManualResetEvent(false);
        internal List<Form> childForms = new List<Form>();
        private int id;

        internal List<IItem> Ships { get; set; }

        Image[] shipImages = {Properties.Resources.Spaceship,
                              Properties.Resources.Camaz,
                              Properties.Resources.Helicopter,
                              Properties.Resources.Plane,
                              Properties.Resources.Taxi,
                              Properties.Resources.Titanic};

        static GUI()
        {
            gameField = GetPanel(750, 500);
        }

        public GUI(int id)
        {
            InitializeComponent();
            this.id = id;
            this.Controls.Add(gameField);

            //здесь форма начинает следить за нажатыми клавишами
            this.PreviewKeyDown += GUI_PreviewKeyDown;
            this.KeyDown += GUI_KeyDown;
            this.KeyUp += GUI_KeyUp;
            //здесь создаем кораблики для формы
            Ships = new List<IItem>();

        }


        private void GUI_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    e.IsInputKey = true;
                    break;
            }
        }
        private void GUI_KeyDown(object sender, KeyEventArgs e)
        {
            if (!T.IsRunning)
            {
                T.Start();
            }
            if (!CheckMove(e) && T.ElapsedMilliseconds > 300 && !waitUntilKeyUp)
            {
                Rebound(e);
                return;

            }

            if (CheckMove(e) && !waitUntilKeyUp)
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
                        Ships[0].MoveUp();
                        break;
                    case Keys.Down:
                        Ships[0].MoveDown();
                        break;
                    case Keys.Left:
                        Ships[0].MoveLeft();
                        break;
                    case Keys.Right:
                        Ships[0].MoveRight();
                        break;
                    case Keys.Space:
                        return;
                    case Keys.A:
                        MessageService.ShowMessage("Это пиздец, работает:))");
                        return;
                }
                SendData?.Invoke(this, JsonConvert.SerializeObject(Ships[0]));
                PrintElements();
            }
        }

        private void GUI_KeyUp(object sender, KeyEventArgs e)
        {
            T.Reset();
            waitUntilKeyUp = false;
        }
        private void btnChoose_Click(object sender, EventArgs e)
        {
            Form form;
            RunForm(Forms.FormChoose, 0, 0, out form);
        }
        internal void RunForm(Forms forms, int x, int y, out Form form)
        {
            form = default;
            switch (forms)
            {
                case Forms.WaitingForm:
                    form = new WaitingForm();
                    waiter.Reset();
                    break;
                case Forms.FormChoose:
                    IItem ship = null;
                    //do
                    //{
                    ship = Ships.Where(el => el.Id == this.id).FirstOrDefault();
                    //} while (ship != null);
                    form = new FormChoose(ref ship);
                    form.Closing += ChooseForm_Closing;
                    waiter.Set();
                    break;
            }
            childForms.Add(form);
            this.Enabled = false;
            form.StartPosition = FormStartPosition.Manual;
            form.Location = new Point(this.Width / 2 - x, this.Height / 2 - y); 
            form.Show();
            //waiter.WaitOne();
        }
        internal void CloseForm(Form form)
        {
            form.Invoke(new MethodInvoker(form.Close));
            this.Enabled = true;
            this.Focus();
        }
        private void ChooseForm_Closing(object sender, CancelEventArgs e)
        {
            SendData?.Invoke(this, JsonConvert.SerializeObject(Ships[0]));
            gameField.Controls.Add(GetShipPicture((IImage)Ships[0]));
            this.Enabled = true;
            btnChoose.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            Refresh();
        }
        private static Panel GetPanel(int height, int width)
        {
            Panel panel = new Panel()
            {
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(12, 12),
                Name = "gameField",
                Size = new Size(height, width),
                TabIndex = 4,
                TabStop = true
            };
            return panel;
        }
        private PictureBox GetShipPicture(IImage image)
        {

            PictureBox picture = new PictureBox()
            {
                BorderStyle = BorderStyle.FixedSingle,
                Image = shipImages[image.Image - 1],
                Location = new Point(image.X, image.Y),
                Name = "pictureBox",
                Size = new Size(image.Size[0], image.Size[1]),
                SizeMode = PictureBoxSizeMode.StretchImage,
                TabIndex = 0,
                TabStop = false
            };
            return picture;
        }
        internal void PrintElements()
        {
            gameField.Controls.Clear();
            foreach (var el in Ships)
            {
                if (el.Image != 0)
                {
                    gameField.Controls.Add(GetShipPicture((IImage)el));
                    Refresh();
                }

            }
        }





        private bool CheckMove(KeyEventArgs e = null)
        {
            //#region Move Conditions.
            //// 4 Условия для возможности совершить заданное пользователем приращение координаты.
            //bool Ship_Step_Down = Ships[0].Y + Ships[0].Size[0] + Ships[0].dS <= gameField.Size.Height;
            //bool Ship_Step_Up = Ships[0].Y - Ships[0].dS >= 0;
            //bool Ship_Step_Left = Ships[0].X - Ships[0].dS >= 0;
            //bool Ship_Step_Right = Ships[0].X + Ships[0].Size[0] + Ships[0].dS <= gameField.Size.Width;

            //// Условия описывающие совпадение соответствующей координаты у обоих кораблей.
            //bool MatchCoordinates_X = Ships[0].X == Ships[1].X || Ships[0].X - Ships[0].dS == Ships[1].X || Ships[0].X + Ships[0].dS == Ships[1].X;
            //bool MatchCoordinates_Y = Ships[0].Y == Ships[1].Y || Ships[0].Y - Ships[0].dS == Ships[1].Y || Ships[0].Y + Ships[0].dS == Ships[1].Y;

            //// 4 Условия для возможности совершить заданное пользователем приращение координаты 
            //// с учетом текущей координаты 2-го корабля. 
            //bool Ship_on_Ship_Down = Ships[0].Y + Ships[1].Size[1] != Ships[1].Y | !MatchCoordinates_X;
            //bool Ship_on_Ship_Up = Ships[0].Y - Ships[0].Size[0] != Ships[1].Y | !MatchCoordinates_X;
            //bool Ship_on_Ship_Left = Ships[0].X - Ships[1].Size[1] != Ships[1].X | !MatchCoordinates_Y;
            //bool Ship_on_Ship_Right = Ships[0].X + Ships[0].Size[0] != Ships[1].X | !MatchCoordinates_Y;
            //#endregion

            //switch (e.KeyCode)
            //{
            //    case (Keys.Up):
            //        return (Ship_on_Ship_Up & Ship_Step_Up); 
            //    case (Keys.Down):
            //        return (Ship_on_Ship_Down & Ship_Step_Down);
            //    case (Keys.Left):
            //        return (Ship_on_Ship_Left & Ship_Step_Left);
            //    case (Keys.Right):
            //        return (Ship_on_Ship_Right & Ship_Step_Right);
            //    default:
            //        return false;
            //}
            return true;
        }

        private void Rebound(KeyEventArgs e)
        {

            bool Xrebound = true;
            bool Yrebound = true;
            switch (e.KeyCode)
            {
                case Keys.Up:
                    if (Yrebound)
                    {
                        Ships[0].MoveDown();
                        Ships[0].MoveDown();
                        Ships[0].MoveDown();
                        break;
                    }
                    else return;
                case Keys.Down:
                    if (Yrebound)
                    {
                        Ships[0].MoveUp();
                        Ships[0].MoveUp();
                        Ships[0].MoveUp();
                        break;
                    }
                    else return;
                case Keys.Left:
                    if (Xrebound)
                    {
                        Ships[0].MoveRight();
                        Ships[0].MoveRight();
                        Ships[0].MoveRight();
                        break;
                    }
                    else return;
                case Keys.Right:
                    if (Xrebound)
                    {
                        Ships[0].MoveLeft();
                        Ships[0].MoveLeft();
                        Ships[0].MoveLeft();
                        break;
                    }
                    else return;
            }
            SendData?.Invoke(this, JsonConvert.SerializeObject(Ships[0]));
            PrintElements();
            waitUntilKeyUp = true;
        }
    }

    internal enum Forms
    {
        WaitingForm,
        FormChoose
    }

}
