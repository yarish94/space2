﻿using MyStarWars.Items;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStarWars.UI
{
    public partial class FormChoose : Form
    {
        IItem ship;
        public FormChoose(ref IItem spaceship)
        {
            InitializeComponent();
            this.ship = spaceship;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ship.Image = (int) Images.First;
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            ship.Image = (int)Images.Second;
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ship.Image = (int)Images.Third;
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            ship.Image = (int)Images.Fourth;
            this.Close();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            ship.Image = (int)Images.Fifth;
            this.Close();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            ship.Image = (int)Images.Sixth;
            this.Close();
        }
    }
}
