﻿using MyStarWars.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MyStarWars.UI
{
    internal partial class WaitingForm : Form
    {
        private static float angle = 30;
        PictureBox waitPic = new PictureBox();
        static Image image = Resources.wait_icon;
        int count = 1;
        internal delegate void Invoker();
        public WaitingForm()
        {
            InitializeComponent();
            var descriptor = this.Handle;
            this.FormClosed += WaitingForm_FormClosed;

            waitPic.Parent = this;
            waitPic.Bounds = new Rectangle(25, 25, 50, 50);
            waitPic.Paint += new PaintEventHandler(WaitPic_Paint);


            waitTimer.Enabled = true;
            waitTimer.Interval = 200;
            waitTimer.Tick += waitTimer_Tick;
            waitTimer.Start();
        }

        private void WaitingForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void WaitPic_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(waitPic.Width / 2, waitPic.Height / 2);
            e.Graphics.RotateTransform(angle);
            e.Graphics.DrawImage(image, -waitPic.Width / 2, -waitPic.Height / 2);
            e.Graphics.RotateTransform(-angle);
            e.Graphics.TranslateTransform(-waitPic.Width / 2, -waitPic.Height / 2);
        }

        private void waitTimer_Tick(object sender, EventArgs e)
        {
            angle += 5.0f;
            waitPic.Invalidate();
        }

        internal void UpdateCount()
        {
            this.count++;
            this.lblCount.Text = $"Попытка №{count}";
        }
        internal void ClearCount()
        {
            this.count = 1;
        }
        
    }
}
