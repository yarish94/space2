﻿using MyStarWars.Client;
using MyStarWars.Items;
using MyStarWars.UI;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStarWars
{
    class Controller
    {

        private readonly GUI view;
        private readonly Model model;
        private readonly MyClient client;
        private static ManualResetEvent resetEvent = new ManualResetEvent(false);
        private static int id = Int32.Parse(DateTime.Now.Millisecond.ToString());

        public Controller()
        {
            view = new GUI(id);
            client = new MyClient(id);
            this.model = new ModelText();
            view.SendData += View_SendData;
            client.InitDone += Client_InitDone;
            client.Update += Client_Update;
            client.ReconnectStarted += Client_ReconnectStarted;
            client.ReconnectFinished += Client_ReconnectFinished;
            Application.Run(view);
        }
        private void Client_InitDone(object sender, EventArgs e)
        {
            view.Ships.Add(new Spaceship(0, GUI.gameField.Size.Height - Spaceship.size[1], id));
            view.Invoke(new MethodInvoker(() => view.Controls.Find("btnChoose", true).FirstOrDefault().Enabled = true));
        }

        private void Client_ReconnectFinished(object sender, EventArgs e)
        {
            resetEvent.Set();
            //client.SendData("Reconnect");
        }

        private async void Client_ReconnectStarted(object sender, EventArgs e)
        {
            resetEvent.Reset();
            await Task.Factory.StartNew(() => RunWaitingForm());
        }
        private void RunWaitingForm()
        {
            Form form = null;
            view.BeginInvoke(new MethodInvoker(() => view.RunForm(Forms.WaitingForm, 0, 0, out form)));
            resetEvent.WaitOne();
            view.BeginInvoke(new MethodInvoker(() => view.CloseForm(form)));
            Thread.Sleep(TimeSpan.FromMilliseconds(50));
        }
        private async void View_SendData(object sender, string e)
        {
            try
            {
                await Task.Run(() => client.SendData(e));
            }
            catch(Exception ex)
            {
                MessageService.ShowError("I have no idea how you appear here unless you are trying to send empty message");
                MessageService.ShowError(ex.Message);
            }
        }

        private void Client_Update(object sender, string e)
        { 
            Spaceship temp = JsonConvert.DeserializeObject<Spaceship>(e.ToString());
            if (temp == null) return;
            var ship = view.Ships.Where(el => el.Id == temp.Id).FirstOrDefault();
            if (ship == null)
            {
                ship = new Spaceship(0, GUI.gameField.Size.Height - Spaceship.size[1], temp.Id);
                view.Ships.Add(ship);
            }
            ship.X = temp.X;
            ship.Y = temp.Y;
            ship.IsAlive = temp.IsAlive;
            ship.Image = temp.Image;
            view.BeginInvoke(new MethodInvoker(view.PrintElements));
        }

        private void View_GameOver(object sender, int e)
        {
            model.SaveScore(e);
            MessageService.ShowMessage("You lost");
        }
    }
}
