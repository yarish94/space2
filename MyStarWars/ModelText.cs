﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStarWars
{
    interface Model
    {
        void SaveScore(int score);

    }
    class ModelText : Model
    {
        private string filePath = Environment.CurrentDirectory.ToString().Replace("bin\\Debug", "Resourses\\Top10Scores.txt");


        public ModelText()
        {
        }

        public void SaveScore(int score)
        {
            List<string> topScores = new List<string>();
            using (StreamReader sr = new StreamReader(filePath))
            {
                topScores = sr.ReadToEnd()
                              .Replace("\r\n", "%")
                              .Split('%')
                              .ToList();
                var temp = new List<string>();
                foreach (var el in topScores)
                {
                    if (!el.Equals("")) temp.Add(el);
                }
                topScores = temp;
            }
            topScores.Add(score.ToString());
            var result = topScores.Select(el => Int32.Parse(el))
                                  .OrderByDescending(el => el)
                                  .Take(10)
                                  .ToList();
            string strOut = null;
            foreach (var el in result)
            {
                strOut += el.ToString();
                strOut += "\r\n";
            }
            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                sw.Write(strOut);
            }
        }
    }
}
