﻿using MyStarWars.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
namespace MyStarWars.Items
{

    public class Spaceship : IImage, IItem
    {
        [JsonProperty("X")]
        public int X { get; set; }
        [JsonProperty("Y")]
        public int Y { get; set; }
        [JsonProperty("IsAlive")]
        public bool IsAlive { get; set; }
        [JsonProperty("Image")]
        public int Image { get; set; }
        [JsonProperty("Id")]
        public int Id { get; set; }

        public static int[] size = new int[2] {50, 50};
        public int[] Size
        {
            get
            {
                return size;
            }
        }
        public int dS
        {
            get
            {
                return 25;
            }
        }

        public Spaceship()
        {
        }

        public Spaceship(int x, int y)
        {
            X = x;
            Y = y;
            IsAlive = true;
        }
        public Spaceship(int x, int y, int id) : this(x, y)
        {
            this.Id = id;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj as Spaceship == null) return false;
            Spaceship second = (Spaceship)obj;
            return this.X == second.X && this.Y == second.Y;
        }
        public void MoveDown()
        {
            Y += dS;
        }

        public void MoveLeft()
        {
            X -= dS;
        }

        public void MoveRight()
        {
            X += dS;
        }

        public void MoveUp()
        {
            Y -= dS;
        }
    }
}
